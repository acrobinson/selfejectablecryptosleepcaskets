# SelfEjectableCryptosleepCaskets [![RimWorld 1.0](https://img.shields.io/badge/RimWorld-1.0-brightgreen.svg)](http://rimworldgame.com/) 

Allows pawns to self eject themselves from cryptosleeep caskets. Can be added to existing save games and removed at anytime.

## Works for:
- Cryptosleep Casket
- Ancient Cryptosleep Casket

## Notes:
To use with an Ancident Crypto Casket, you need to clear the casket and then claim it.
